// Fungsi untuk validasi nama
function validateName(name) {
    // Nama harus terdiri dari huruf alphabet dan spasi
    var regex = /^[a-zA-Z\s]+$/;
    return regex.test(name);
  }
  
  // Fungsi untuk validasi email
  function validateEmail(email) {
    // Menggunakan regular expression untuk memeriksa format email
    var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return regex.test(email);
  }
  
  // Fungsi untuk validasi nomor telepon
  function validatePhoneNumber(phoneNumber) {
    // Nomor telepon harus terdiri dari angka dan panjangnya antara 10-12 digit
    var regex = /^\d{10,12}$/;
    return regex.test(phoneNumber);
  }
  
  // Fungsi untuk mengumpulkan dan memproses biodata
  function submitBiodata(event) {
    event.preventDefault(); // Mencegah halaman refresh saat submit
  
    // Mengambil nilai input dari form
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var phoneNumber = document.getElementById("phone").value;
  
    // Memvalidasi nama
    if (!validateName(name)) {
      alert("Nama tidak valid. Hanya huruf alfabet dan spasi yang diperbolehkan.");
      return;
    }
  
    // Memvalidasi email
    if (!validateEmail(email)) {
      alert("Email tidak valid. Harap periksa format email Anda.");
      return;
    }
  
    // Memvalidasi nomor telepon
    if (!validatePhoneNumber(phoneNumber)) {
      alert("Nomor telepon tidak valid. Hanya angka yang diperbolehkan (10-12 digit).");
      return;
    }
  
    // Jika semua validasi berhasil, tampilkan hasil biodata
    alert("Biodata berhasil dikirim:\n\nNama: " + name + "\nEmail: " + email + "\nNomor Telepon: " + phoneNumber);
  
    // Reset form
    document.getElementById("biodataForm").reset();
  }
  
  // Menambahkan event listener pada form saat submit
  document.getElementById("biodataForm").addEventListener("submit", submitBiodata);
  