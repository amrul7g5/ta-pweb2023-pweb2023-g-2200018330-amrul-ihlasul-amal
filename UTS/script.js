// script.js

// Mendapatkan elemen header dan gambar header
const header = document.querySelector('.header');
const headerImage = document.querySelector('.header-image');

// Array dengan daftar gambar header
const headerImages = [
  'header_image1.jpg',
  'header_image2.jpg',
  'header_image3.jpg',
  'header_image4.jpg'
];

// Indeks saat ini dari gambar header
let currentImageIndex = 0;

// Mengubah gambar header dengan animasi transisi
function changeHeaderImage() {
  headerImage.style.opacity = 0; // Mengubah opacity menjadi 0 (transparan)
  setTimeout(() => {
    currentImageIndex = (currentImageIndex + 1) % headerImages.length; // Memperbarui indeks gambar header
    const newImage = headerImages[currentImageIndex];
    headerImage.style.backgroundImage = `url(${newImage})`; // Mengganti gambar header
    headerImage.style.opacity = 1; // Mengubah opacity menjadi 1 (terlihat)
  }, 300); // Waktu penundaan sebelum mengubah gambar header
}

// Mengganti gambar header setiap 7 detik
setInterval(changeHeaderImage, 5000);
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
  anchor.addEventListener('click', function (e) {
    e.preventDefault();

    document.querySelector(this.getAttribute('href')).scrollIntoView({
      behavior: 'smooth'
    });
  });
});