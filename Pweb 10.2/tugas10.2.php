<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP</title>
    <style>
        /* CSS Styling */
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        h3 {
            margin-bottom: 10px;
        }

        form {
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input[type="number"] {
            padding: 5px;
            width: 200px;
        }

        button[type="submit"] {
            padding: 5px 10px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            cursor: pointer;
        }

        .result {
            margin-bottom: 20px;
            padding: 10px;
            background-color: #e9f5e9;
            border: 1px solid #4CAF50;
            color: #4CAF50;
        }

        .triangle {
            margin-bottom: 20px;
            font-size: 20px;
        }
        
        .refresh-button {
            background-color: #ddd;
            color: #333;
            border: none;
            padding: 5px 10px;
            cursor: pointer;
        }
    </style>
    <script>
        // Function to show the result div
        function showResult(elementId) {
            var resultDiv = document.getElementById(elementId);
            resultDiv.style.display = 'block';

            var submitButton = resultDiv.previousElementSibling.querySelector('button[type="submit"]');
            submitButton.disabled = true;
        }

        // Function to refresh the result
        function refreshResult(elementId) {
            var resultDiv = document.getElementById(elementId);
            resultDiv.style.display = 'none';

            var submitButton = resultDiv.nextElementSibling.querySelector('button[type="submit"]');
            submitButton.disabled = false;
        }
    </script>
</head>
<body>
    <h2>PHP</h2>
    <form method="POST" onsubmit="showResult('resultConvert'); return false;">
        <h3>Konversi Nilai</h3>
        <label for="nilai">Masukkan Nilai:</label>
        <input type="number" name="nilai" id="nilai" required>
        <button type="submit" name="convert">Konversi</button>
    </form>

    <form method="POST" onsubmit="showResult('resultTriangle'); return false;">
        <h3>Segitiga Bintang</h3>
        <label for="tinggi">Masukkan Tinggi:</label>
        <input type="number" name="tinggi" id="tinggi" required>
        <button type="submit" name="triangle">Buat Segitiga</button>
    </form>

    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['convert'])) {
            $nilai = $_POST['nilai'];

            if ($nilai >= 80 && $nilai <= 100) {
                echo '<div id="resultConvert" class="result">Nilai anda 4.00, nilai huruf = A</div>';
            } elseif ($nilai >= 76.25 && $nilai <= 79.99) {
                echo '<div id="resultConvert" class="result">Nilai anda 3.67, nilai huruf = A-</div>';
            } elseif ($nilai >= 68.75 && $nilai <= 76.24) {
                echo '<div id="resultConvert" class="result">Nilai anda 3.33, nilai huruf = B+</div>';
            } elseif ($nilai >= 65.00 && $nilai <= 68.74) {
                echo '<div id="resultConvert" class="result">Nilai anda 3.00, nilai huruf = B</div>';
            } elseif ($nilai >= 62.50 && $nilai <= 64.99) {
                echo '<div id="resultConvert" class="result">Nilai anda 2.67, nilai huruf = B-</div>';
            } elseif ($nilai >= 57.50 && $nilai <= 62.49) {
                echo '<div id="resultConvert" class="result">Nilai anda 2.33, nilai huruf = C+</div>';
            } elseif ($nilai >= 55.00 && $nilai <= 57.49) {
                echo '<div id="resultConvert" class="result">Nilai anda 2.00, nilai huruf = C</div>';
            } elseif ($nilai >= 51.25 && $nilai <= 54.99) {
                echo '<div id="resultConvert" class="result">Nilai anda 1.67, nilai huruf = C-</div>';
            } elseif ($nilai >= 43.75 && $nilai <= 51.24) {
                echo '<div id="resultConvert" class="result">Nilai anda 1.33, nilai huruf = D+</div>';
            } elseif ($nilai >= 40.00 && $nilai <= 43.74) {
                echo '<div id="resultConvert" class="result">Nilai anda 1.00, nilai huruf = D</div>';
            } else {
                echo '<div id="resultConvert" class="result">Nilai anda 0.00, nilai huruf = E</div>';
            }
            echo '<button class="refresh-button" onclick="refreshResult(\'resultConvert\')">Refresh</button>';
        } elseif (isset($_POST['triangle'])) {
            $tinggi = $_POST['tinggi'];

            if ($tinggi >= 1) {
                echo '<div id="resultTriangle" class="triangle">';
                for ($baris = 1; $baris <= $tinggi; $baris++) {
                    // Buat sejumlah spasi
                    for ($i = 1; $i < $tinggi - $baris; $i++) {
                        echo "&nbsp;"; // Karakter spasi
                    }

                    // Tampilkan * sebanyak (2 * baris - 1)
                    for ($j = 1; $j < 2 * $baris; $j++) {
                        echo "*";
                    }

                    // Pindah baris
                    echo "<br>";
                }
                echo '</div>';
                echo '<button class="refresh-button" onclick="refreshResult(\'resultTriangle\')">Refresh</button>';
            } else {
                echo '<div class="result">Masukkan tinggi yang lebih besar dari 0.</div>';
            }
        }
    }
    ?>

</body>
</html>
