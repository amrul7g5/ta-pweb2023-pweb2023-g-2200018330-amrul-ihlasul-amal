<?php
// Kode untuk menambah saldo pengguna

session_start();

$id = $_SESSION['id'];
$amount = $_POST['amount'];

// Fungsi untuk mendapatkan saldo dari file (gunakan kode yang sesuai)
function getSaldoFromFile($id) {
    $accounts = file("akun.txt");

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            return $accountData[2];
        }
    }

    return 0; // Jika ID tidak ditemukan, kembalikan saldo 0
}

// Fungsi untuk memperbarui saldo dalam file (gunakan kode yang sesuai)
function updateSaldoInFile($id, $saldo) {
    $accounts = file("akun.txt");
    $fileContent = "";

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            $accountData[2] = $saldo;
        }
        $fileContent .= implode(" ", $accountData);
    }

    file_put_contents("akun.txt", $fileContent);
}

// Mendapatkan saldo pengguna (gunakan kode yang sesuai)
$saldo = getSaldoFromFile($id);

// Tambahkan saldo
$saldo += $amount;

// Perbarui saldo (gunakan kode yang sesuai)
updateSaldoInFile($id, $saldo);

// Kembalikan respons dalam format JSON
$response = [
    'message' => 'Saldo berhasil ditambahkan.',
    'saldo' => $saldo
];

header('Content-Type: application/json');
echo json_encode($response);
?>
