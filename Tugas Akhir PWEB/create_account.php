<?php
// Kode untuk membuat akun baru

$id = $_POST['id'];
$password = $_POST['password'];

// Fungsi untuk memeriksa apakah ID sudah digunakan sebelumnya
function isIDExist($id) {
    $accounts = file("akun.txt");

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            return true;
        }
    }

    return false;
}

// Periksa apakah ID sudah digunakan sebelumnya
if (isIDExist($id)) {
    echo "ID sudah digunakan sebelumnya. Silakan pilih ID lain.";
} else {
    // Tambahkan data akun ke file
    $newAccount = $id . " " . $password . " 0" . PHP_EOL;
    file_put_contents("akun.txt", $newAccount, FILE_APPEND);

    echo "Akun berhasil dibuat. ID dan password telah disimpan.";
}
?>
