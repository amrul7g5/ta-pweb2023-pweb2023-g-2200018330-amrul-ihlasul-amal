<?php
session_start();

$id = $_POST['id'];
$password = $_POST['password'];

// Fungsi untuk memeriksa apakah ID dan password valid
function verifyAccount($id, $password) {
    $accounts = file("akun.txt");

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id && $accountData[1] == $password) {
            return true;
        }
    }

    return false;
}

// Verifikasi ID dan password
if (verifyAccount($id, $password)) {
    $_SESSION['id'] = $id;
    header("Location: dashboard.php");
    exit;
} else {
    echo "<script>alert('ID atau password tidak valid.'); window.location.href = 'index.html';</script>";
}
?>
