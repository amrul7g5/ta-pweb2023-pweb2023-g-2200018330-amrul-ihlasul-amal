function showCekSaldoForm() {
    document.getElementById('cek-saldo-form').style.display = 'block';
    document.getElementById('cek-saldo-info').textContent = '';
}

function hideCekSaldoForm() {
    document.getElementById('cek-saldo-form').style.display = 'none';
    document.getElementById('cek-saldo-info').textContent = '';
}

function showTambahSaldoForm() {
    document.getElementById('tambah-saldo-form').style.display = 'block';
    document.getElementById('tambah-saldo-info').textContent = '';
}

function hideTambahSaldoForm() {
    document.getElementById('tambah-saldo-form').style.display = 'none';
    document.getElementById('tambah-saldo-info').textContent = '';
}

function showTransferForm() {
    document.getElementById('transfer-form').style.display = 'block';
    document.getElementById('transfer-info').textContent = '';
}

function hideTransferForm() {
    document.getElementById('transfer-form').style.display = 'none';
    document.getElementById('transfer-info').textContent = '';
}

function showTarikTunaiForm() {
    document.getElementById('tarik-tunai-form').style.display = 'block';
    document.getElementById('tarik-tunai-info').textContent = '';
}

function hideTarikTunaiForm() {
    document.getElementById('tarik-tunai-form').style.display = 'none';
    document.getElementById('tarik-tunai-info').textContent = '';
}

function tambahSaldo() {
    var amount = document.getElementById('tambah-saldo-amount').value;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            document.getElementById('tambah-saldo-info').textContent = response.message;
            document.getElementById('saldo-info').textContent = response.saldo;
        }
    };
    xhr.open('POST', 'tambah_saldo.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('amount=' + amount);
}

function transfer() {
    var tujuanID = document.getElementById('tujuan-transfer-id').value;
    var amount = document.getElementById('transfer-amount').value;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            document.getElementById('transfer-info').textContent = response.message;
            document.getElementById('saldo-info').textContent = response.saldo;
        }
    };
    xhr.open('POST', 'transfer.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('tujuanID=' + tujuanID + '&amount=' + amount);
}

function tarikTunai() {
    var amount = document.getElementById('tarik-tunai-amount').value;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            document.getElementById('tarik-tunai-info').textContent = response.message;
            document.getElementById('saldo-info').textContent = response.saldo;
        }
    };
    xhr.open('POST', 'tarik_tunai.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send('amount=' + amount);
}
