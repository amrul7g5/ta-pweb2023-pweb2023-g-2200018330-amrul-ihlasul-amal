<?php
// Kode untuk mendapatkan saldo pengguna

session_start();

$id = $_SESSION['id'];

// Fungsi untuk mendapatkan saldo dari file (gunakan kode yang sesuai)
function getSaldoFromFile($id) {
    $accounts = file("akun.txt");

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            return $accountData[2];
        }
    }

    return 0; // Jika ID tidak ditemukan, kembalikan saldo 0
}

// Mendapatkan saldo pengguna (gunakan kode yang sesuai)
$saldo = getSaldoFromFile($id);

// Kembalikan respons dalam format JSON
$response = [
    'saldo' => $saldo
];

header('Content-Type: application/json');
echo json_encode($response);
?>
