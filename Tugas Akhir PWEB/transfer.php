<?php
// Kode untuk transfer saldo

session_start();

$id = $_SESSION['id'];
$tujuanID = $_POST['tujuanID'];
$amount = $_POST['amount'];

// Fungsi untuk mendapatkan saldo dari file (gunakan kode yang sesuai)
function getSaldoFromFile($id) {
    $accounts = file("akun.txt");

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            return $accountData[2];
        }
    }

    return 0; // Jika ID tidak ditemukan, kembalikan saldo 0
}

// Fungsi untuk memperbarui saldo dalam file (gunakan kode yang sesuai)
function updateSaldoInFile($id, $saldo) {
    $accounts = file("akun.txt");
    $fileContent = "";

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            $accountData[2] = $saldo;
        }
        $fileContent .= implode(" ", $accountData);
    }

    file_put_contents("akun.txt", $fileContent);
}

// Mendapatkan saldo pengguna (gunakan kode yang sesuai)
$saldo = getSaldoFromFile($id);
$saldoTujuan = getSaldoFromFile($tujuanID);

// Periksa apakah saldo mencukupi untuk transfer
if ($saldo >= $amount) {
    // Lakukan transfer
    $saldo -= $amount;
    $saldoTujuan += $amount;

    // Perbarui saldo (gunakan kode yang sesuai)
    updateSaldoInFile($id, $saldo);
    updateSaldoInFile($tujuanID, $saldoTujuan);

    // Kirim respons
    $response = [
        'message' => 'Transfer berhasil.',
        'saldo' => $saldo
    ];
} else {
    $response = [
        'message' => 'Saldo tidak mencukupi untuk transfer.',
        'saldo' => $saldo
    ];
}

header('Content-Type: application/json');
echo json_encode($response);
?>
