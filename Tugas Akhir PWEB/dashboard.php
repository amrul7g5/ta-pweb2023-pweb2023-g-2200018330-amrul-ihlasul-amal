<?php
session_start();

$id = $_SESSION['id'];

// Fungsi untuk mendapatkan saldo dari file
function getSaldoFromFile($id) {
    $accounts = file("akun.txt");

    foreach ($accounts as $account) {
        $accountData = explode(" ", $account);
        if ($accountData[0] == $id) {
            return $accountData[2];
        }
    }

    return 0; // Jika ID tidak ditemukan, kembalikan saldo 0
}

// Mendapatkan saldo pengguna
$saldo = getSaldoFromFile($id);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Mobile Banking - Dashboard</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="script.js"></script>
</head>
<body>
    <div id="dashboard-container">
        <h2>Selamat Datang, <?php echo $id; ?>!</h2><br>
        <button type="button" onclick="toggleMenu('cek-saldo-form')"><img src="cek_saldo_logo.png" alt="Cek Saldo"> Cek Saldo</button>
        <div id="cek-saldo-form" style="display: none;">
            <h3>Saldo Anda: Rp. <span id="saldo-info"><?php echo $saldo; ?></span></h3>
        </div>
        <button type="button" onclick="toggleMenu('tambah-saldo-form')"><img src="tambah_saldo_logo.png" alt="Tambah Saldo"> Tambah Saldo</button>
        <div id="tambah-saldo-form" style="display: none;">
            <form>
                <input type="number" id="tambah-saldo-amount" placeholder="Jumlah" required>
                <button type="button" onclick="tambahSaldo()">Tambah Saldo</button>
                <p id="tambah-saldo-info"></p>
            </form>
        </div>
        <button type="button" onclick="toggleMenu('transfer-form')"><img src="transfer_logo.png" alt="Transfer"> Transfer</button>
        <div id="transfer-form" style="display: none;">
            <form>
                <input type="text" id="tujuan-transfer-id" placeholder="ID Tujuan" required>
                <input type="number" id="transfer-amount" placeholder="Jumlah" required>
                <button type="button" onclick="transfer()">Transfer</button>
                <p id="transfer-info"></p>
            </form>
        </div>
        <button type="button" onclick="toggleMenu('tarik-tunai-form')"><img src="tarik_tunai_logo.png" alt="Tarik Tunai"> Tarik Tunai</button>
        <div id="tarik-tunai-form" style="display: none;">
            <form>
                <input type="number" id="tarik-tunai-amount" placeholder="Jumlah" required>
                <button type="button" onclick="tarikTunai()">Tarik Tunai</button>
                <p id="tarik-tunai-info"></p>
            </form>
        </div>
        <a href="logout.php">Logout</a>
    </div>

    <script>
        function toggleMenu(menuId) {
            var menu = document.getElementById(menuId);
            var menus = document.querySelectorAll('[id$="-form"]');
            for (var i = 0; i < menus.length; i++) {
                if (menus[i].id !== menuId) {
                    menus[i].style.display = 'none';
                }
            }
            menu.style.display = (menu.style.display === 'none') ? 'block' : 'none';
        }
    </script>
</body>
</html>

